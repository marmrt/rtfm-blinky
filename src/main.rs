//#![deny(unsafe_code)]
#![no_main]
#![no_std]

extern crate panic_semihosting;
extern crate stm32f4;

use rtfm::{app};
use stm32f4::stm32f411;

const PERIOD: u32 = 8_000_000;
#[app(device = stm32f4::stm32f411)]
const APP: () = {
    // resources
    static mut gpioa: stm32f411::GPIOA = ();
    static mut tim2: stm32f411::TIM2 = ();
    static mut ON: bool = false;
    #[init]
    fn init() {
        let p: stm32f411::Peripherals = device;

        p.RCC.ahb1enr.modify( |_,w| w.gpioaen().enabled());
        p.RCC.apb1enr.modify(|_,w| w.tim2en().enabled());

        p.TIM2.cr1.modify(|_,w| w.cen().set_bit().udis().clear_bit());
        p.TIM2.dier.modify(|_,w| w.uie().set_bit());
        unsafe {
            p.TIM2.arr.modify(|_,w| w.bits(PERIOD));
        }

        let gpioa_init = p.GPIOA;
        gpioa_init.moder.modify(|_,w| w.moder5().output());
        gpioa_init.pupdr.modify(|_,w| w.pupdr5().pull_up());
        gpioa = gpioa_init;
        tim2 = p.TIM2;
    }

    #[interrupt (resources=[tim2,gpioa,ON])]
    fn TIM2() {
        if *resources.ON == true { // turn off LED
            resources.gpioa.bsrr.write(|w| w.br5().reset());
            *resources.ON = false;
        }
        else { // Turn LED on
            resources.gpioa.bsrr.write(|w| w.bs5().set());
            *resources.ON = true;
        }
        resources.tim2.sr.modify(|_,w| w.uif().clear_bit());
    }

    extern "C" {
        fn TIM3();
    }
};

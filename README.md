# RTFM Blink
A blinking LED program for the NUcleo-F411RE board. First commit uses the RTFM scheduler while later commits uses TIM2 interrupts to control the frequency.

# License

This template is licensed under either of

- Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or
  http://www.apache.org/licenses/LICENSE-2.0)

- MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.
